const seo = {
  title: "Prasad's Portfolio",
  description: "A passionate web devloper",
};

const greeting = {
  title: "Prasad Tawde",
  logo: "PrasadTawde",
  description: "A passionate web devloper",
};

const socials = [
  {
    name: "Github",
    link: "https://github.com/PrasadTawde",
  },
  {
    name: "LinkedIn",
    link: "https://github.com/PrasadTawde",
  },
];

export { seo, greeting, socials };
