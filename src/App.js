import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/style.css";
import Home from "./pages/home";

function App() {
  return <Home />;
}

export default App;
