import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <p className="footer-text">Prasad Tawde</p>
      </div>
    );
  }
}

export default Footer;
